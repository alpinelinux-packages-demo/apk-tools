# apk-tools
Alpine Package Keeper - package manager. https://pkgs.alpinelinux.org/packages?name=apk-tools&arch=x86_64

# Official documentation
* [man 8 apk](https://man.archlinux.org/man/apk.8.en)

# Semi-official documentation
* [*Comparison with other distros*
  ](https://wiki.alpinelinux.org/wiki/Comparison_with_other_distros)
* [apk flags](https://gist.github.com/sgreben/dfeaaf20eb635d31e1151cb14ea79048)
* [*Alpine Linux in a chroot*
  ](https://wiki.alpinelinux.org/wiki/Alpine_Linux_in_a_chroot)
* [*Creating an Alpine package*
  ](https://wiki.alpinelinux.org/wiki/Creating_an_Alpine_package)
# Unofficial documentation
* [*Installing APK in Alpine Linux*
  ](https://www.baeldung.com/linux/apk-alpine-install)
  2024-08
